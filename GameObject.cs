﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace SpaceInvaders
{
    public class GameObject
    {
        protected Vector2 position;
        protected Texture2D sprite;
        protected float rotationAngle;
        protected float scaling;
        protected Color color; 


        public GameObject(Vector2 position, Texture2D sprite)
        {
            this.position = position;
            this.sprite = sprite;
            this.rotationAngle = 0.0f;
            this.scaling = 1.0f;
            this.color = Color.White;
        }

        public virtual void Update(GameTime gameTime)
        {
            /*
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            float rotationSpeed = MathHelper.Pi / 2;
            rotationAngle += rotationSpeed * dt;*/
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            // Make sure the position of the sprite is at the center of where it draws
            Vector2 spriteCenter = new Vector2(sprite.Bounds.Center.X, sprite.Bounds.Center.Y);
            
            spriteBatch.Draw(sprite, position, null, color * (color.A / 255.0f), rotationAngle, spriteCenter, scaling, SpriteEffects.None, 0f);
        }

        public Vector2 GetPosition()
        {
            return position;
        }
    }
}

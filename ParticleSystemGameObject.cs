﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SpaceInvaders
{
    class ParticleSystemGameObject : PhysicsGameObject
    {
        class Particle
        {
            public float currentTime;
            public float currentSize;
            public float currentAngle;
            public float angularVelocity;
            public Color currentColor;
            public Vector2 currentPosition;
            public Vector2 currentVelocity;

            public Particle()
            {
            }

            public void Update(GameTime gameTime, ParticleSystemGameObject ps)
            {
                float dt = gameTime.ElapsedGameTime.Milliseconds / 1000.0f;
                float t = currentTime / ps.lifetime;

                // Update time, position, angle
                currentTime += dt;
                currentPosition += currentVelocity * dt;
                currentAngle += angularVelocity * dt;

                // Interpolate Values
                if (t < 1.0f)
                {
                    // Color
                    currentColor.R = (byte)MathHelper.Lerp((float)ps.initialColor.R, (float)ps.finalColor.R, t);
                    currentColor.G = (byte)MathHelper.Lerp((float)ps.initialColor.G, (float)ps.finalColor.G, t);
                    currentColor.B = (byte)MathHelper.Lerp((float)ps.initialColor.B, (float)ps.finalColor.B, t);
                    currentColor.A = (byte)MathHelper.Lerp((float)ps.initialColor.A, (float)ps.finalColor.A, t);

                    // Interpolate Size
                    currentSize = MathHelper.Lerp(ps.initialSize, ps.finalSize, currentTime / ps.lifetime);
                }

            }

        }

        List<Particle> particleList = new List<Particle>();

        // descriptor
        public float newParticleEmittedCountdown = 0.0f;
        public float emissionRate;
        public float lifetime;
        public float initialSize, finalSize;
        public Color initialColor, finalColor;
        public Vector2 initialVelocity;

        Random rng;

        public ParticleSystemGameObject(Vector2 position, Texture2D sprite, int rngSeed)
            : base(position, sprite, new Vector2(0, 0), new Vector2(0, 0))
        {
            newParticleEmittedCountdown = 0.0f;
            emissionRate = 10f;
            lifetime = 2.5f;
            initialSize = 10.0f;
            finalSize = 128.0f;
            initialColor = Color.Blue;
            finalColor = Color.White;
            finalColor.A = 0;
            initialVelocity = new Vector2(-100.0f, 0.0f);

            rng = new Random(rngSeed);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // Emit particles
            if (newParticleEmittedCountdown < 0.0f)
            {
                newParticleEmittedCountdown = (1.0f / emissionRate) + (float)(rng.NextDouble() - 0.5f) / emissionRate;
                Particle particle = new Particle();

                Vector2 mousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
                position.X = mousePos.X;
                position.Y = 960;
                particle.currentColor = initialColor;
                particle.currentVelocity = initialVelocity + new Vector2((float)(rng.NextDouble() * 50 - 25), (float)(rng.NextDouble() * 70 - 35));
                particle.currentPosition = position;
                particle.currentAngle = (float) (rng.NextDouble() * 2 * Math.PI);
                particle.angularVelocity = (float)(rng.NextDouble() - 0.5f) * 2f;
                particle.currentTime = 0.0f;
                particleList.Add(particle);
            }

            newParticleEmittedCountdown -= gameTime.ElapsedGameTime.Milliseconds / 1000.0f;

            // Update Particles
            for (int i = 0; i < particleList.Count; ++i)
            {
                Particle p = particleList[i];
                p.Update(gameTime, this);

                // Remove particle if lifetime expired
                if (p.currentTime > lifetime)
                {
                    particleList.RemoveAt(i);
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            // Make sure the position of the sprite is at the center of where it draws
            foreach (Particle particle in particleList)
            {
                Vector2 spriteCenter = new Vector2(sprite.Bounds.Center.X, sprite.Bounds.Center.Y);
                scaling = particle.currentSize / sprite.Bounds.Size.X;

                spriteBatch.Draw(sprite, particle.currentPosition, null, particle.currentColor * (particle.currentColor.A/255f), particle.currentAngle, spriteCenter, scaling, SpriteEffects.None, 0f);
            }
        }

    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace SpaceInvaders
{
    public class PhysicsGameObject : GameObject
    {
        protected Vector2 velocity;
        protected Vector2 acceleration;
        protected float radius;

        public PhysicsGameObject(Vector2 position, Texture2D sprite, 
                                 Vector2 velocity, Vector2 acceleration)
        : base(position, sprite)
        {
            this.velocity = velocity;
            this.acceleration = acceleration;

            // Calculate radius from sprite size
            radius = sprite.Bounds.Width / 2;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // Apply Gravity and Update Velocity and Position from Acceleration
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Determine new Velocty and Position
            velocity += acceleration * dt;
            position += velocity * dt;

        }

        public bool collidesWith(PhysicsGameObject other)
        {
            Vector2 v = position - other.position;
            double distance = v.Length();

            // Detected collision
            return (distance < radius + other.radius);
        }

        public void handleCollisionWith(PhysicsGameObject other)
        {
            Vector2 n = other.position - position;
            float distance = n.Length();
            float overlap = distance - radius - other.radius;
            n.Normalize();
            Vector2 t = new Vector2(-n.Y, n.X);

            // Separate the circles
            position += (overlap / 2) * n;
            other.position -= (overlap / 2) * n;

            // Velocities according to n and t
            Vector2 v1t = Vector2.Dot(velocity, t) * t;
            Vector2 v1n = Vector2.Dot(velocity, n) * n;
            Vector2 v2t = Vector2.Dot(other.velocity, t) * t;
            Vector2 v2n = Vector2.Dot(other.velocity, n) * n;

            // Velocities after collision
            velocity = v1t + v2n;
            other.velocity = v2t + v1n;
        }
    }
}

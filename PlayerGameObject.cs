﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
namespace SpaceInvaders
{
    class PlayerGameObject : GameObject
    {
        public PlayerGameObject(Vector2 position, Texture2D sprite)
            : base(position, sprite)
        {

        }


        public override void Update(GameTime gameTime)
        {
            // TODO: Rotate cannon towards mouse cursor
            Vector2 mousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            /* float dx = mousePos.X - position.X;
            float dy = mousePos.Y - position.Y;
            //rotationAngle = (float) Math.Atan2(dy, dx); */
            position.X = mousePos.X;

            base.Update(gameTime);
        }
    }
}

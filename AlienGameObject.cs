﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace SpaceInvaders
{
    class AlienGameObject : GameObject
    {
        private bool isDestroyed;
        private double destructionTime;
        private bool right;
        public Guid identifier;


        public AlienGameObject(Vector2 position, Texture2D sprite)
            : base(position, sprite)
        {
            isDestroyed = false;
            identifier = Guid.NewGuid();
        }

        public void Destroy(GameTime gameTime)
        {
            isDestroyed = true;
            destructionTime = gameTime.TotalGameTime.TotalSeconds;
        }

        public bool isTargetDestroyed()
        {
            return isDestroyed;
        }

        public void flagRight(bool flag)
        {
            right = flag;
        }

        public bool getRight()
        {
            return right;
        }

        public void move(bool @override)
        {
            if(@override)
            {
                moveDown();
                return;
            }

            if (right)
            {
                moveRight();
            }
            else
            {
                moveLeft();
            }
        }

        public void moveRight()
        {
            position.X += 1;
        }

        public void moveLeft()
        {
            position.X -= 1;
        }

        public void moveDown()
        {
            position.Y += 10;
        }

        public override void Update(GameTime gameTime)
        {              
            if (isDestroyed)
            {
                const float SHRINKING_TIME = 0.6f;
                float t = (float) (gameTime.TotalGameTime.TotalSeconds - destructionTime) / SHRINKING_TIME;

                if (t < 1.0)
                {
                    scaling = MathHelper.Lerp(1.0f, 0.0f, t);
                    color.A = (byte)MathHelper.Lerp(255.0f, 0.0f, t);
                }
                else
                {
                    scaling = 0.0f;
                    color.A = 0;
                }
            }

        }

    }
}

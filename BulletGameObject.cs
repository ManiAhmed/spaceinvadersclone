﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace SpaceInvaders
{
    class BulletGameObject : PhysicsGameObject
    {
        public enum BulletOrigins { Player, Alien };
        public BulletOrigins origin;
        public Guid identifier;

        public BulletGameObject(Vector2 position, Texture2D sprite, BulletOrigins origin)
            : base(position, sprite, new Vector2(0,0), new Vector2(0,0))
        {
            identifier = Guid.NewGuid();
            this.origin = origin;
            if (origin == BulletOrigins.Player)
            {
                Vector2 mousePosition = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
                velocity = mousePosition - position;
                velocity.Normalize();
                velocity *= 300;
            }
            else
            {
                Vector2 alienPos = new Vector2(0, position.Y);
                velocity = alienPos;
                velocity.Normalize();
                velocity *= 300;
            }
        }

        
        public override void Update(GameTime gameTime)
        {
            // base.Update will change the position
            // Before calling it, we get the position on the last frame
            // After calling it, we get the position on the current frame
           float lastX = position.X;

            base.Update(gameTime);

            float currentX = position.X;
        }

    }
}

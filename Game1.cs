﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
    using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;
using static SpaceInvaders.BulletGameObject;

namespace SpaceInvaders
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        List<GameObject> gameObjectList = new List<GameObject>();
        List<GameObject> newList = new List<GameObject>();

        Texture2D laser;
        Texture2D playerShip;
        Texture2D target;
        Texture2D heart;
        Texture2D background;
        Texture2D smoke;

        Song gameMusic;
        Song gameWinningMusic;
        Song gameLostMusic;

        SoundEffect cannonSound;
        SoundEffect explosion;
        AlienGameObject leftmost;
        AlienGameObject rightmost;
        float lastShotTime = 0;
        int borderXright = 900;
        int borderXleft = 50;
        int playerLives = 3;
        int score = 0;

        public enum GameState
        {
            StartMenu,
            Playing
        };

        GameState state;

        List<Guid> guids = new List<Guid>();


        SpriteFont font;

        KeyboardState keyState = Keyboard.GetState();

        //    Vector2 cannonBallPosition;
        MouseState prevMouseState;
        int width, height;
        int targetCount = 15;
        bool wonGame = false;
        bool lostGame = false;
        
        

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.PreferredBackBufferWidth = 500;
            graphics.PreferredBackBufferHeight = 1000;
            IsMouseVisible = true;
            Content.RootDirectory = "Content";

            prevMouseState = Mouse.GetState();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            state = GameState.StartMenu;
            width = graphics.PreferredBackBufferWidth;
            height = graphics.PreferredBackBufferHeight;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            laser = Content.Load<Texture2D>("laser");
            playerShip = Content.Load<Texture2D>("spaceship2");
            target = Content.Load<Texture2D>("al2");
            font = Content.Load<SpriteFont>("gameFont");
            gameMusic = Content.Load<Song>("background");
            gameWinningMusic = Content.Load<Song>("win");
            gameLostMusic = Content.Load<Song>("lose");
            cannonSound = Content.Load<SoundEffect>("test");
            explosion = Content.Load<SoundEffect>("explosion");
            heart = Content.Load<Texture2D>("heart");
            background = Content.Load<Texture2D>("nebula");
            smoke = Content.Load<Texture2D>("star");

            gameObjectList.Add(new PlayerGameObject(new Vector2(50, 960), playerShip));

            gameObjectList.Add(new AlienGameObject(new Vector2(100, 100), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(250, 150), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(400, 100), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(550, 150), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(700, 100), target));

            gameObjectList.Add(new AlienGameObject(new Vector2(100, 200), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(250, 250), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(400, 200), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(550, 250), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(700, 200), target));

            gameObjectList.Add(new AlienGameObject(new Vector2(100, 300), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(250, 350), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(400, 300), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(550, 350), target));
            gameObjectList.Add(new AlienGameObject(new Vector2(700, 300), target));

            gameObjectList.Add(new ParticleSystemGameObject(new Vector2(50, 960), smoke, 1));
            gameObjectList.Add(new GameObject(new Vector2(50, 50), heart));
            gameObjectList.Add(new GameObject(new Vector2(150, 50), heart));
            gameObjectList.Add(new GameObject(new Vector2(250, 50), heart));
            leftmost = (AlienGameObject)gameObjectList[1];
            rightmost = (AlienGameObject)gameObjectList[targetCount];

            Random rng = new Random();

            MediaPlayer.Play(gameMusic);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (state == GameState.StartMenu && Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                state = GameState.Playing;
                base.Draw(gameTime);
            }

            int currentLives = playerLives;
            
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            Random rng = new Random();
            var time = (float)gameTime.TotalGameTime.TotalSeconds - lastShotTime;
            if (((float)gameTime.TotalGameTime.TotalSeconds - lastShotTime) >= 0.8)
            {
                time = (float)gameTime.TotalGameTime.TotalSeconds - lastShotTime;
                var alienList = gameObjectList.Where(_ =>
                    {
                        if (_.GetType() != typeof(AlienGameObject)) return false;

                        var alien = (AlienGameObject)_;
                        return alien.isTargetDestroyed() == false;
                    }
                );
                
                int index = rng.Next(0, targetCount);
                if(alienList.Count() != 0)
                    gameObjectList.Add(new BulletGameObject(alienList.ElementAt(index).GetPosition(), laser, BulletOrigins.Alien));
                lastShotTime = (float)gameTime.TotalGameTime.TotalSeconds;
            }

            var @override = (rightmost.GetPosition().X + target.Width > borderXright || leftmost.GetPosition().X < borderXleft);
            borderXright = 900;
            borderXleft = 50;
            for (int k = 0; k < gameObjectList.Count; k++)
            {
                GameObject gameObject = gameObjectList[k];
                if (gameObject.GetType() == typeof(AlienGameObject))
                {
                    AlienGameObject alien = (AlienGameObject)gameObject;
                    alien.move(@override);

                    if (@override)
                    {
                        alien.flagRight(!alien.getRight());
                        borderXleft = -1;
                        borderXright = int.MaxValue;
                    }

                    Vector2 v = gameObjectList[0].GetPosition() - alien.GetPosition();
                    double distance = Math.Sqrt(v.X * v.X + v.Y * v.Y);

                    // Detected collision
                    if (distance < playerShip.Width / 2 + target.Width / 2)
                    {
                        playerLives--;
                        if (playerLives == 0)
                        {
                            targetCount = 0;
                            MediaPlayer.Stop();
                            MediaPlayer.Play(gameLostMusic);
                            lostGame = true;
                            gameObjectList.Clear();
                            break;
                        }
                    }

                }
            }
      
            if (Mouse.GetState().LeftButton == ButtonState.Pressed &&
                prevMouseState.LeftButton != ButtonState.Pressed)
            {
                if(gameObjectList.Count() > 0) { 
                    GameObject cannon = gameObjectList[0];
                    Vector2 pos = cannon.GetPosition();
                    gameObjectList.Add(new BulletGameObject(pos, laser, BulletOrigins.Player));
                
                    // Adjust the random pitch +- 0.05
                    cannonSound.Play(1.0f, (float)rng.NextDouble() * 0.1f - 0.05f, 0.0f);
                }
            }

            for (int i = 0; i < gameObjectList.Count; i++)
            {
                GameObject gameObject = gameObjectList[i];
                gameObject.Update(gameTime);

                if (gameObject.GetType() == typeof(AlienGameObject))
                {
                    var bulletList = gameObjectList.Where(_ =>
                   {
                       if (_.GetType() != typeof(BulletGameObject)) return false;

                       var bullet = (BulletGameObject)_;

                       return bullet.origin == BulletOrigins.Player;
                   });

                    foreach (GameObject projectile in bulletList)
                    {
                        AlienGameObject targetObject = (AlienGameObject)gameObject;
                        if (targetObject.isTargetDestroyed() == false)
                        {
                            Vector2 v = gameObject.GetPosition() - projectile.GetPosition();
                            double distance = Math.Sqrt(v.X * v.X + v.Y * v.Y);

                            // Detected collision
                            if (distance < laser.Width / 2 + target.Width / 2)
                            {
                                if (targetObject.identifier.Equals(leftmost.identifier) || targetObject.identifier.Equals(rightmost.identifier))
                                {
                                    Task check = new Task(() =>
                                    {

                                        var targetList = gameObjectList.Where(_ => _.GetType() == typeof(AlienGameObject)).OrderBy(_ => _.GetPosition().X);
                                        leftmost = (AlienGameObject)targetList.First();
                                        rightmost = (AlienGameObject)targetList.Last();
                                    });

                                    check.Start();
                                }
                                //gameObjectList.Remove(gameObject);
                                explosion.Play();
                                score += 100;
                                targetObject.Destroy(gameTime);
                                targetCount--;
                                break;
                            }
                        }
                    }
                }

                if (gameObject.GetType() == typeof(PlayerGameObject))
                {

                    var bulletListAlien = gameObjectList.Where(_ =>
                    {
                        if (_.GetType() != typeof(BulletGameObject)) return false;

                        var bullet = (BulletGameObject)_;

                        return bullet.origin == BulletOrigins.Alien;
                    });

                    foreach (GameObject projectile in bulletListAlien)
                    {
                        PlayerGameObject playerObject = (PlayerGameObject)gameObject;
                        Vector2 v = gameObject.GetPosition() - projectile.GetPosition();
                        double distance = Math.Sqrt(v.X * v.X + v.Y * v.Y);

                        // Detected collision
                        if (distance < laser.Width / 2 + target.Width / 2)
                        {
                            BulletGameObject hitProj = (BulletGameObject) projectile;
                            guids.Add(hitProj.identifier);
                            Console.WriteLine(hitProj.identifier);
                            playerLives--;
                            
                        }
                    }
                    
                }

                
                if (wonGame == false && targetCount == 0)
                {
                    MediaPlayer.Stop();
                    MediaPlayer.Play(gameWinningMusic);
                    wonGame = true;
                }

                if (playerLives == 0)
                {
                    targetCount = 0;
                    MediaPlayer.Stop();
                    MediaPlayer.Play(gameLostMusic);
                    lostGame = true;
                    gameObjectList.Clear();
                }


                prevMouseState = Mouse.GetState();
                base.Update(gameTime);
            }


            gameObjectList = gameObjectList.Where(_ =>
            {
                if (_.GetType() != typeof(BulletGameObject)) return true;
                var bullet = (BulletGameObject)_;
                return !(guids.Contains(bullet.identifier));
            }).ToList();

            if (currentLives != playerLives)
            {
                int indexCount = 0;
                if (gameObjectList.Count() > 0)
                {
                    foreach (GameObject heart in gameObjectList)
                    {
                        if (heart.GetType() == typeof(GameObject))
                        {
                            break;
                        }
                        else
                        {
                            indexCount++;
                        }
                    }
                    gameObjectList.RemoveAt(indexCount);
                }
                
            }
                
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Bisque);

            spriteBatch.Begin();
            spriteBatch.Draw(background, new Rectangle(0, 0, 1000, 1000), Color.White);

            if(state == GameState.StartMenu)
            {
                spriteBatch.DrawString(font, "PRESS ENTER TO START", new Vector2(250, 200), Color.Black);
            }

            else if (state == GameState.Playing)
            {
                spriteBatch.DrawString(font, "Score: " + score, new Vector2(300, 50), Color.Black);

                foreach (GameObject gameObject in gameObjectList)
                {
                    gameObject.Draw(spriteBatch, gameTime);
                }

                // Draw cannons last
                if (gameObjectList.Count >= 2)
                {
                    gameObjectList[0].Draw(spriteBatch, gameTime);
                    gameObjectList[1].Draw(spriteBatch, gameTime);
                }

                if (wonGame)
                {
                    spriteBatch.DrawString(font, "You win!!!", new Vector2(300, 200), Color.Black);
                }
                
                else if(lostGame)
                {
                    spriteBatch.DrawString(font, "Game Over!", new Vector2(300, 200), Color.Black);
                }
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
